Rails.application.routes.draw do

  resources :feed_items

  resources :asks

  resources :tickets

  resources :special_links

  root 'static_pages#home'
  
  match '/tickets/:ticket_id/message', to: 'ticket_messages#create', via: 'post', as: :create_ticket_message

  match '/tickets/:ticket_id/message/:id', to: 'ticket_messages#destroy', via: 'delete', as: :ticket_message

  match '/tickets/:ticket_id/message/:id', to: 'ticket_messages#update', via: 'patch'

  match '/tickets/:ticket_id/message/:id', to: 'ticket_messages#update', via: 'put'

  match '/funds/:id/finish_fund', to: 'funds#finish_fund', via: 'get', as: :finish_fund


  resources :tickets do
    member do
      get 'close'
      get 'open'
    end
    resources :ticket_messages, only: [:create, :update, :destroy]
  end
  
  match '/list_news', to: 'feed_items#full_list', via: 'get'

  match '/news_feed', to: 'feed_items#index', via: 'get'

  match '/l/:special_link', to: 'special_links#record', via: 'get'

  match '/l/:special_link/:user_hash', to: 'special_links#record', via: 'get'

  match '/funds/current', to: 'funds#current', via: 'get'

  match '/funds/finished', to: 'funds#finished', via: 'get'

  match '/withdraw', to: 'users#withdraw_money', via: 'get'

  match '/settings/:id', to: 'users#edit', via: 'get'

  match '/settings', to: 'users#edit', via: 'get'

  match '/settings', to: 'users#update', via: 'patch'
  
  match '/settings', to: 'users#update', via: 'put'

  match '/invited_list/:id', to: 'users#invited_list', via: 'get'

  match '/invited_list', to: 'users#invited_list', via: 'get'

  match '/next', to: 'users#next_registration', via: 'get'

  match '/signup', to: 'users#new', via: 'get'

  match '/check_response', to: 'users#check_response', via: 'get'

  match '/signin', to: 'sessions#new', via: 'get'
  
  match '/signout', to: 'sessions#destroy', via: 'delete'

  match '/help', to: 'static_pages#help', via: 'get'

  match '/how_it_work', to: 'static_pages#how_it_work', via: 'get'
  
  match '/profile', to: 'static_pages#profile', via: 'get'

  match '/project_achievements', to: 'static_pages#project_achievements', via: 'get'

  match '/statistics', to: 'static_pages#statistics', via: 'get'

  resources :funds

  resources :users
  resources :sessions, only: [:new, :create, :destroy]

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
