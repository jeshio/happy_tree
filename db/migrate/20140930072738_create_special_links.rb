class CreateSpecialLinks < ActiveRecord::Migration
  def change
    create_table :special_links do |t|
      t.string :name
      t.string :special_link
      t.string :icon

      t.timestamps
    end
  end
end
