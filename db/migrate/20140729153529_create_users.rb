class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :login, null: false
      t.string :password_digest
      t.string :email
      t.boolean :email_get_news
      t.string :remember_token
      t.integer :inviter_id
      t.boolean :anonimity
      t.boolean :goodness
      t.float :capital
      t.float :deposit
      t.string :language
      t.boolean :confirmed
      t.boolean :fund, unique: true
      t.boolean :admin, unique: true
      t.string :user_hash
      t.integer :special_link_id
      t.integer :step

      t.timestamps
    end

    add_index :users, :login, unique: true
    add_index :users, :inviter_id
    add_index :users, :user_hash, unique: true
  end
end
