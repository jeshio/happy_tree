class CreateFeedItems < ActiveRecord::Migration
  def change
    create_table :feed_items do |t|
      t.string :title
      t.text :content
      t.boolean :ready

      t.timestamps
    end
  end
end
