class CreateFunds < ActiveRecord::Migration
  def change
    create_table :funds do |t|
      t.string :name
      t.text :content
      t.string :ewallet
      t.text :response
      t.float :result_other
      t.float :result
      t.boolean :finished
      t.timestamp :begin_time
      t.timestamp :end_time

      t.timestamps
    end
  end
end
