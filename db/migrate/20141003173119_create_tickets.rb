class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :user_id
      t.string :title, limit: 200
      t.text :content, limit: 2000
      t.integer :priority, limit: 1
      t.integer :ticket_status, limit: 1

      t.timestamps
    end
  end
end
