class CreateAsks < ActiveRecord::Migration
  def change
    create_table :asks do |t|
      t.string :content
      t.string :response
      t.integer :priority
      t.integer :ask_status

      t.timestamps
    end
  end
end
