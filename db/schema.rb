# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150311104430) do

  create_table "asks", force: :cascade do |t|
    t.string   "content",    limit: 255
    t.string   "response",   limit: 255
    t.integer  "priority",   limit: 4
    t.integer  "ask_status", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "feed_items", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "content",    limit: 65535
    t.boolean  "ready",      limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "funds", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.text     "content",      limit: 65535
    t.string   "ewallet",      limit: 255
    t.text     "response",     limit: 65535
    t.float    "result_other", limit: 24
    t.float    "result",       limit: 24
    t.boolean  "finished",     limit: 1
    t.datetime "begin_time"
    t.datetime "end_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "special_links", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "special_link", limit: 255
    t.string   "icon",         limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ticket_messages", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "ticket_id",  limit: 4
    t.text     "content",    limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tickets", force: :cascade do |t|
    t.integer  "user_id",       limit: 4
    t.string   "title",         limit: 200
    t.text     "content",       limit: 65535
    t.integer  "priority",      limit: 1
    t.integer  "ticket_status", limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "login",           limit: 255, null: false
    t.string   "password_digest", limit: 255
    t.string   "email",           limit: 255
    t.boolean  "email_get_news",  limit: 1
    t.string   "remember_token",  limit: 255
    t.integer  "inviter_id",      limit: 4
    t.boolean  "anonimity",       limit: 1
    t.boolean  "goodness",        limit: 1
    t.float    "capital",         limit: 24
    t.float    "deposit",         limit: 24
    t.string   "language",        limit: 255
    t.boolean  "confirmed",       limit: 1
    t.boolean  "fund",            limit: 1
    t.boolean  "admin",           limit: 1
    t.string   "user_hash",       limit: 255
    t.integer  "special_link_id", limit: 4
    t.integer  "step",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phoneNumber",     limit: 255
  end

  add_index "users", ["inviter_id"], name: "index_users_on_inviter_id", using: :btree
  add_index "users", ["login"], name: "index_users_on_login", unique: true, using: :btree
  add_index "users", ["user_hash"], name: "index_users_on_user_hash", unique: true, using: :btree

end
