class User < ActiveRecord::Base
	attr_accessor :invite_key, :current_password

	ADMIN_PC  = 0.10
	INVITER_PC  = 0.50
	INVITER_2_PC  = 0.10
	INVITER_3_PC  = 0.10
	INVITER_4_PC  = 0.10

	INVITE_BONUS = 0.5 # дополнительное вознаграждение от части админа за регистрацию по приглашению

	has_many :childrens, foreign_key: "inviter_id", class_name: "User"
	has_many :tickets,  dependent: :destroy
	accepts_nested_attributes_for :tickets,
		:allow_destroy => true

	before_validation :trim_fileds

	belongs_to :inviter, class_name: "User"

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.*[a-z\d]+)*\.[a-z]+\z/i
	VALID_LOGIN_REGEX =  /\A([а-яА-Яa-zA-Z0-9\-_]{5,20})\z/

	validates :login, presence: true, length: { minimum: 5, maximum: 20 }, uniqueness: { case_sensitive: false }, format: { with: VALID_LOGIN_REGEX }
	validates :email, presence: true, if: :step_other_info?
	validates :email, allow_blank: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false },
				length: { maximum: 50 }
	validates :invite_key, allow_blank: true, length: { minimum: 3, maximum: 7 }
	validates :deposit, presence: true, inclusion: { in: [100], message: "неверное значение депозита" }, if: :step_other_info?
	validate :check_invite_key
	validate :check_password_update, on: :update

	has_secure_password
	validates :password, allow_blank: true, length: { minimum: 6, maximum: 60 }

	before_save do
		self.email.downcase! unless self.email.blank?
	end

	before_create do
		create_remember_token
		invite_key_to_id unless self.invite_key.blank?
		self.inviter_id = User.first.id if self.inviter_id.nil? && !User.first.nil?
		self.step = 1 if self.step.nil?
		self.confirmed = 0 if self.confirmed.nil?
		self.user_hash = gen_user_hash
		self.capital = 0
		self.confirmed = 0 if self.confirmed.nil?
		self.admin = true if User.count === 0
	end

	before_update do
		self.email = nil if self.email.blank?

		share_money_after_activating unless self.inviter.nil?
	end

	def User.new_remember_token
		SecureRandom.urlsafe_base64
	end

	def User.encrypt(token)
		Digest::SHA1.hexdigest(token.to_s)
	end

	def User.find_by_login(login)
		User.where("lower(login) = ?", login.downcase).first
	end

	def opened_tickets
		Ticket.opened(self.id)
	end

	def tickets_opened_to_user
		Ticket.opened_to_user(self.id)
	end

	def tickets_opened_to_admin
		Ticket.opened_to_admin(self.id)
	end

	def confirmed_childrens
		self.childrens.where(confirmed: true)
	end

	# находит ближайшего Goodness
	def get_goodnesses
		res = []
		i = 0
		inviter = self
		res.push(-1) if inviter.goodness
		while i < self.parents_count
			inviter = inviter.inviter
			res.push(i) if inviter.goodness
			i += 1
		end
		res
	end

	# есть ли среди ближайших родителей админ
	def has_admin?
		res = 0
		inviter = self
		while res < parents_count
			inviter = inviter.inviter
			return true if inviter.admin?
			res += 1
		end
		false
	end

	def trim_fileds
		self.login.strip! unless self.login.nil?
		self.email.strip! unless self.email.nil?
	end

	def get_parent(level = 0)
		i = 0
		inviter = self
		while i <= level
			inviter = inviter.inviter
			i += 1
		end
		inviter
	end

	# посчитать количество родителей с пределом счёта
	def parents_count(limit = 4)
		res = 0
		inviter = self.inviter
		while res < limit && !inviter.nil?
			inviter = inviter.inviter
			res += 1
		end
		res
	end

	private 

		def create_remember_token
			self.remember_token = User.encrypt(User.new_remember_token)
		end

		def invite_key_to_id
	        user = User.find_by(user_hash: self.invite_key)
        	self.inviter_id = user.id
	        self.invite_key = nil
	    end

	    def check_invite_key
	    	unless self.invite_key.blank?
	    		user = User.find_by(user_hash: self.invite_key)
	    		if user.nil? || !user.confirmed?
	    			errors.add(:invite_key, 'неверен')
	    		end
	    	end
	    end

	    def gen_user_hash
			len = 3

			result = random_string 3

			user_count = User.count

			len += 1 while 36**len < user_count/2

			until User.find_by(user_hash: result).nil?
				result = random_string(len)
			end

			result
		end

		def random_string(len = 3)
			arr = ('a'..'z').to_a + (0..9).to_a
			(0..len-1).map { (arr)[rand(arr.count)] }.join.to_s
		end

		def step_other_info?
			!self.step.nil? && self.step == 1
		end

		def check_password_update
			unless self.password.blank?
				unless User.find(self.id).authenticate(self.current_password)
					errors.add(:current_password, "неверный текущий пароль")
				end
			end
		end

		def step_last?
			@user = User.find_by_id(self.id)
			!@user.nil? && self.confirmed? && !@user.confirmed?
		end

		def share_money_after_activating
			@user = User.find_by_id(self.id)

			# те, кто пожелал отдать все деньги в фонд
			goodnesses = @user.get_goodnesses

			if !errors.any? && !@user.nil? && self.confirmed? && !@user.confirmed?
				deposit = self.deposit

				unless self.inviter.nil?
					# отдаём часть пригласившему
					# условие, что пригласивший не Goodness
					# кроме этого, админ отдаёт свою часть в фонд, если в этой ветке есть Goodness
					if !self.inviter.goodness? && !(!goodnesses.empty? && self.inviter.admin?)
						self.inviter.capital += self.deposit * INVITER_PC

						deposit -= self.deposit * INVITER_PC
					end

					self.inviter.capital += self.deposit * ADMIN_PC * INVITE_BONUS if !self.inviter.admin? && self.inviter.created_at > 7.days.ago

					self.inviter.save!

					inviter_2 = self.get_parent(1)

					unless inviter_2.nil?
						if !inviter_2.goodness? && !(!goodnesses.empty? && inviter_2.admin?)
							inviter_2.capital += self.deposit * INVITER_2_PC

							deposit -= self.deposit * INVITER_2_PC
						end

						inviter_3 = inviter_2.inviter

						inviter_2.save!

						unless inviter_3.nil?
							if !inviter_3.goodness? && !(!goodnesses.empty? && inviter_3.admin?)
								inviter_3.capital += self.deposit * INVITER_3_PC

								deposit -= self.deposit * INVITER_3_PC
							end

							inviter_4 = inviter_3.inviter

							inviter_3.save!

							unless inviter_4.nil?
								if !inviter_4.goodness? && !(!goodnesses.empty? && inviter_4.admin?)
									inviter_4.capital += self.deposit * INVITER_4_PC

									deposit -= self.deposit * INVITER_4_PC
								end
						
								inviter_4.save!
							
							end
						end
					end
				end

				if !@user.has_admin? && !User.first.nil? && goodnesses.empty?
					money = User.first.capital + self.deposit * ADMIN_PC

					money -= self.deposit * ADMIN_PC * INVITE_BONUS if self.inviter.created_at > 7.days.ago

					User.first.update_attribute(:capital, money)
					deposit -= self.deposit * ADMIN_PC
				else
					if !User.first.nil? && !self.admin? && !self.inviter.admin? && self.inviter.created_at > 7.days.ago
						deposit -= self.deposit * ADMIN_PC * INVITE_BONUS
					end
				end

				Fund.add_to_current(deposit)
			end
		end
end
