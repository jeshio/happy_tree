class FeedItem < ActiveRecord::Base
	before_create do
		self.ready = false if self.ready.nil?
	end

	def FeedItem.prepared
		FeedItem.where(ready: true)
	end

	def FeedItem.unprepared
		FeedItem.where(ready: false)
	end

	def FeedItem.last_items(items = 10)
		FeedItem.where(ready: true).order("id desc").limit(items).all.reverse
	end
end
