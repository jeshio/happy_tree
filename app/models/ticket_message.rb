class TicketMessage < ActiveRecord::Base
	belongs_to :tickets
	accepts_nested_attributes_for :tickets

	validates :content, presence: true, length: { minimum: 1, maximum: 600 }
	validates :user_id, presence: true
	validates :ticket_id, presence: true

end