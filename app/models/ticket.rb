class Ticket < ActiveRecord::Base
	belongs_to :creater, foreign_key: "user_id", class_name: "User"
	has_many :ticket_messages,  dependent: :destroy
	accepts_nested_attributes_for :ticket_messages,
		:allow_destroy => true

	validates :title, presence: true, length: { minimum: 6, maximum: 200 }
	validates :content, presence: true, length: { minimum: 20, maximum: 1800 }
	validates :priority, presence: true
	validates :user_id, presence: true
	validate :priority_validate

	CLOSED = 0
	USER_WAIT = 1
	ADMIN_WAIT = 2

	before_create do
		self.ticket_status = USER_WAIT if self.ticket_status.nil?
		self.priority = 0 if self.priority.nil?
	end

	def closed?
		self.ticket_status == CLOSED
	end

	def set_status_by_user(user_id)
		user = User.find(user_id)
		if user.admin?
			self.ticket_status = USER_WAIT
		else
			self.ticket_status = ADMIN_WAIT
		end
	end

	def Ticket.closed(user_id)
		Ticket.where('user_id = ? AND ticket_status = ?', user_id, CLOSED)
	end

	def Ticket.opened(user_id)
		Ticket.where('user_id = ? AND ticket_status != ?', user_id, CLOSED)
	end

	def Ticket.opened_to_admin(user_id)
		Ticket.where('user_id = ? AND ticket_status = ?', user_id, USER_WAIT)
	end

	def Ticket.opened_to_user(user_id)
		Ticket.where('user_id = ? AND ticket_status = ?', user_id, ADMIN_WAIT)
	end

	private
		def priority_validate
			unless self.priority.nil?
				errors.add(:priority, "неверный приоритет") unless self.priority.between?(CLOSED, ADMIN_WAIT + 1)
			end
		end
end
