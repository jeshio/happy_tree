class SpecialLink < ActiveRecord::Base

	validates :name, presence: true, length: { maximum: 30 }, uniqueness: { case_sensitive: false }
	validates :special_link, presence: true, length: { maximum: 30 }, uniqueness: { case_sensitive: false }
	validates :icon, allow_blank: true, length: { maximum: 70 }

end
