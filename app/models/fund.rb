class Fund < ActiveRecord::Base
	validates :name, presence: true, length: { minimum: 5, maximum: 220 }
	validates :content, presence: true, length: { minimum: 5, maximum: 800 }
	validates :begin_time, presence: true
	validates :end_time, presence: true
	validate :check_time_begin_greate_end, unless: :skip_callbacks
	validate :check_time_intersection, unless: :skip_callbacks
	validate :check_time_old, on: :create, unless: :skip_callbacks

	before_create do
		self.finished = 0
		self.result = 0
		self.result_other = 0
	end

	def Fund.add_to_current(money)
		result = Fund.where("begin_time < ? AND end_time > ?", Time.now, Time.now).first()

		if result.nil?		
			result = Fund.where("begin_time < ?", Time.now).last

			if result.nil?
				result = Fund.last

				if result.nil?
					User.first.update_attribute(:capital, User.first.capital + money)
					return
				end
			end
		end

		result.update_attribute(:result, result.result + money)

	end

	# последний фонд, которому была оказана помощь
	def Fund.last_helped
		Fund.where("end_time < ?", Time.now).order(:end_time).last()
	end

	def Fund.finished
		Fund.where("end_time < ?", Time.now).order(:end_time)
	end

	def Fund.non_finished
		Fund.where("end_time > ?", Time.now).order(:begin_time)
	end

	def finish_it
		self.update_attribute(:finished, 1)
	end

	private
		def check_time_begin_greate_end
			if self.begin_time > self.end_time
				errors.add(:begin_time, 
					"дата начала не может быть позже даты окончания")
			end
		end

		def check_time_old
			if self.begin_time < Date.today
				errors.add(:begin_time, 
					"дата начала не может быть раньше текущего дня")
			end
		end

		def check_time_intersection
			b = self.begin_time
			e = self.end_time

			intesect_begin = 
			Fund.where("begin_time < ? AND end_time > ?", b, b).first()

    		intesect_end = 
    		Fund.where("begin_time < ? AND end_time > ?", e, e).first()

    		unless intesect_begin.nil?
    			errors.add(:begin_time, 
					"дата начала пересекается с датой уже добавленного фонда '#{intesect_begin.name}'
					(#{intesect_begin.begin_time} - #{intesect_begin.end_time})")
    		end

    		unless intesect_end.nil?
    			errors.add(:end_time, 
					"дата окончания пересекается с датой уже добавленного фонда '#{intesect_end.name}'
					(#{intesect_end.begin_time} - #{intesect_end.end_time})")
    		end
		end
end
