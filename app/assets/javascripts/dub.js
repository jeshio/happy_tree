$(document).ready(function () {
	var imgWidth = 228;
	var x = 0, y = 0;

	var time = 1;
	var timeWait = 2500;

	var obj = $('#fund-dub-bot');

	var text = $('#fund-info .text');

	var timer;

	var meter = $('#fund-info-meter');

    obj.css('backgroundPosition', x + 'px' + ' ' + y + '%');

    text.hide();

	timer = createMoving();

	function createMoving () {
		return window.setInterval(function() {
			if (x > 0 && x % imgWidth == 0) {
				x = imgWidth;
				text.show(200);
				clearInterval(timer);
				setTimeout(function(){
					text.hide();
					meter.css('width', 0);
					setTimeout(function(){
					    timer = createMoving();
					}, timeWait/2);
				}, timeWait);
			};
	        obj.css("backgroundPosition", x + 'px' + ' ' + y + '%');
	        meter.css('width', Math.floor(x % (imgWidth + 1) / imgWidth * 100) + "%");
	        x++;
	    }, time);
	}

	// смена цвета в списке
	var ul = $('#get-money ul');

	gradientList(ul);

	// закрашивает лист в градиент зелёного цвета
	function gradientList (ul) {
		ul.each(function (i) {
			li = $(ul[i]).find('li');
			var greenColor = 0;

			li.each(function (j) {
				$(li[j]).css('color', 'rgb(0, ' + greenColor + ', 0)');
				greenColor += Math.round(220/(li.length - 1));
			});
		});
	}
	    
});