// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require turbolinks
//= require cocoon

$(function(){ $(document).foundation(); });

$( document ).ready(function() {
	function move_bg(obj, y, time) {
		var x = 0;

	    obj.css('backgroundPosition', x + 'px' + ' ' + y + '%');  

		window.setInterval(function() {  
	        obj.css("backgroundPosition", x + 'px' + ' ' + y + '%');
	        x--;

	    }, time);
	}
	move_bg($('#sky'), 30, 40);
	move_bg($('#sky-2'), 40, 30);

	// END Background

	make_footer();
});

function make_footer () {
	var footer = $("#footer");
    var pos = footer.position();
    var height = $(window).height();
    height = height - pos.top;
    height = height - footer.height();
    if (height > 0) {
        footer.css({
            'margin-top': height + 'px'
        });
    }
}
