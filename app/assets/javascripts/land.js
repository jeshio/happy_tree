$( document ).ready(function() {
	var landUpdateTime = 3000;
	var landInterval = true;
	var curActive = [];

	// ** стираем все таймеры
	var id = window.setTimeout(function() {}, 0);

	while (id--) {
	    window.clearTimeout(id);
	}
	// **
	
	function setLandActive (block) {
		if (curActive.length > 0 && block[0] == curActive[0]) {
			return 0;
		};

		curActive = block;

		$('.land-active').each(function () {
			$(this).removeClass('land-active').find('.land-list').fadeOut(500);
			$(this).removeClass('land-active-text');
		});

		block.addClass('land-active');

		if ($(window).width() >= 1000) {
			block.addClass('land-active-text');
		}

		block.find('.land-list').fadeIn(0);
	}
	function setLandActiveNext() {
		var active = $('.land-active').last().next('.land-item');
		if (!active.length) {
			active = $('.land-item').first();
		}
		setLandActive(active);
	}
	
	function startInterval () {
		if (landInterval)
			setLandActiveNext();
		setTimeout(startInterval, landUpdateTime);
	}

	startInterval();

	$('.land-item').click(function () {
		landInterval = false;
		setLandActive($(this));
	});
	
	$('.land-item').mouseover(function () {
		landInterval = false;
		setLandActive($(this));
	});

	$('#land-image').mouseout(function () {
		landInterval = true;
	});

	landMarginFix();

	$(window).resize(function() {
	  landMarginFix();
	});

	function landMarginFix () {
		var margin = '10px';
		if ($(window).width() >= 1000) {
			$('.land-active').each(function () {
				$(this).addClass('land-active-text');
			});
			$('.land-item').removeClass('land-min');
			margin = '290px';
		} else {
			$('.land-active').each(function () {
				$(this).removeClass('land-active-text');
			});
			$('.land-item').each(function () {
				$(this).addClass('land-min');
			});
			margin = '400px';
		};
		$('.land-item').css('margin-top', margin);
	}
});