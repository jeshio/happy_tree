class StaticPagesController < ApplicationController

  before_action :signed_in_user, only: [:profile, :statistics, :help]
  
  before_action :admin_user, only: :statistics

  before_action :check_confirmation,  only: [:profile, :help]

  def home
      @newsFeed = FeedItem.prepared.order('created_at desc').paginate(page: params[:page], per_page: 5)
      redirect_to :profile if signed_in?
  end

  def profile
    @user = current_user
    @newsFeed = FeedItem.prepared.order('created_at desc').paginate(page: params[:page], per_page: 5)

    render 'users/profile'
  end

  def how_it_work
    @asks = Ask.where(ask_status: 1).order('priority DESC') 
  end

  def help
    @asks = Ask.where(ask_status: 1).order('priority DESC')
  end

  def project_achievements
    @user_count = User.count
  end

  def statistics

  end

end
