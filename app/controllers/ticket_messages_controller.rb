class TicketMessagesController < ApplicationController
  before_action :signed_in_user
  before_action :admin_user,         except: [:create]
  before_action :check_confirmation

	before_action :set_ticket
  before_action :to_closed_ticket
  before_action :check_ticket
	before_action :set_ticket_message, except: [:create]

  def create
  	@ticket_message = TicketMessage.new(ticket_message_params)

  	@ticket_message.user_id = current_user.id

  	@ticket_message.ticket_id = @ticket.id

  	if @ticket_message.save
      @ticket.set_status_by_user(@ticket_message.user_id)

      @ticket.save
    end

    redirect_to @ticket
  end

  def update
  	@ticket_message.update
  	
  	redirect_to @ticket_message
  end

  def destroy
  	ticket_id = @ticket_message.ticket_id
  	
  	@ticket_message.destroy
  	
  	redirect_to(ticket_path(ticket_id))
  end

  private
  
  	def set_ticket
      @ticket = Ticket.find(params[:ticket_id])
    end

    def check_ticket
      unless current_user.admin?
        redirect_to(root_url) unless current_user.tickets.include?(@ticket)
      end
    end

  	def set_ticket_message
      @ticket_message = TicketMessage.find(params[:id])
    end

    def ticket_message_params
      params.require(:ticket_message).permit(:content)
    end

    def to_closed_ticket
      redirect_to @ticket if @ticket.closed?
    end
end
