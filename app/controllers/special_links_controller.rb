class SpecialLinksController < ApplicationController
  before_action :signed_in_user,      except: [:record]
  before_action :non_signed_in_user,  only:   [:record]
  before_action :admin_user,          except: [:record]

  before_action :set_special_link,    only:   [:show, :edit, :update, :destroy]


  # GET /special_links
  # GET /special_links.json
  def index
    @special_links = SpecialLink.all.paginate(page: params[:page], per_page: 10)
  end

  # GET /special_links/1
  # GET /special_links/1.json
  def show
  end

  # GET /special_links/new
  def new
    @special_link = SpecialLink.new
  end

  # GET /special_links/1/edit
  def edit
  end

  def record
    special_link = SpecialLink.find_by(special_link: params[:special_link])
    
    set_special_link_id(special_link.id) unless special_link.nil?

    set_inviter_hash(params[:user_hash])

    redirect_to root_url
  end

  # POST /special_links
  # POST /special_links.json
  def create
    @special_link = SpecialLink.new(special_link_params)

    respond_to do |format|
      if @special_link.save
        format.html { redirect_to @special_link, notice: 'Special link was successfully created.' }
        format.json { render :show, status: :created, location: @special_link }
      else
        format.html { render :new }
        format.json { render json: @special_link.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /special_links/1
  # PATCH/PUT /special_links/1.json
  def update
    respond_to do |format|
      if @special_link.update(special_link_params)
        format.html { redirect_to @special_link, notice: 'Special link was successfully updated.' }
        format.json { render :show, status: :ok, location: @special_link }
      else
        format.html { render :edit }
        format.json { render json: @special_link.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /special_links/1
  # DELETE /special_links/1.json
  def destroy
    @special_link.destroy
    respond_to do |format|
      format.html { redirect_to special_links_url, notice: 'Special link was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_special_link
      @special_link = SpecialLink.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def special_link_params
      params.require(:special_link).permit(:name, :special_link, :icon)
    end
end
