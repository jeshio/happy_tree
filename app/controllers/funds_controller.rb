class FundsController < ApplicationController
  before_action :signed_in_user,   except: [:current, :finished]
  before_action :admin_user,       except: [:current, :finished]
  before_action :set_fund,         only: [:show, :edit, :update, :destroy, :finish_fund]

  # GET /funds
  # GET /funds.json
  def index
    @funds = Fund.all.paginate(page: params[:page], per_page: 5)
  end

  # GET /funds/1
  # GET /funds/1.json
  def show
  end

  def current
    @fund = Fund.
    where("begin_time < ? AND end_time > ?", Time.now, Time.now).
    first()

    @last_helped = Fund.last_helped

    @current = true
    
    render 'show'
  end

  def finished
    @funds = Fund.finished.paginate(page: params[:page], per_page: 5)
  end

  def finish_fund
  end

  # GET /funds/new
  def new
    @fund = Fund.new
  end

  # GET /funds/1/edit
  def edit
  end

  # POST /funds
  # POST /funds.json
  def create
    @fund = Fund.new(fund_params)

    respond_to do |format|
      if @fund.save
        format.html { redirect_to @fund, notice: 'Fund was successfully created.' }
        format.json { render :show, status: :created, location: @fund }
      else
        format.html { render :new }
        format.json { render json: @fund.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /funds/1
  # PATCH/PUT /funds/1.json
  def update
    respond_to do |format|
      if @fund.update(fund_params)
        format.html { redirect_to @fund, notice: 'Fund was successfully updated.' }
        format.json { render :show, status: :ok, location: @fund }
      else
        format.html { render :edit }
        format.json { render json: @fund.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /funds/1
  # DELETE /funds/1.json
  def destroy
    @fund.destroy
    respond_to do |format|
      format.html { redirect_to funds_url, notice: 'Fund was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fund
      @fund = Fund.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fund_params
      params.require(:fund).permit(:name, :content, :ewallet, :response, :result, :result_other, :finished, :begin_time, :end_time)
    end
end
