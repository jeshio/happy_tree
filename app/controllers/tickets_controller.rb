class TicketsController < ApplicationController
  before_action :signed_in_user
  before_action :admin_user,          except: [:new, :index, :show, :create, :close, :open]
  before_action :check_confirmation

  before_action :set_ticket,          only: [:show, :edit, :update, :destroy, :open, :close]
  before_action :set_ticket_message,  only: [:show]
  before_action :only_current_user,   only: [:show, :close, :open]

  # GET /tickets
  # GET /tickets.json
  def index
    if current_user.admin?
      @tickets = Ticket.all.order('ticket_status desc, priority desc, created_at desc')
    else
      @tickets = current_user.tickets.order('ticket_status desc, priority desc, created_at desc')
    end
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
  end

  def close
    @ticket.update(ticket_status: 0)
    redirect_to @ticket
  end

  def open
    @ticket.update(ticket_status: 1)
    redirect_to @ticket
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new
  end

  # GET /tickets/1/edit
  def edit
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Ticket.new(ticket_params)
    @ticket.user_id = current_user.id

    respond_to do |format|
      if @ticket.save
        format.html { redirect_to @ticket, notice: 'Ticket was successfully created.' }
        format.json { render :show, status: :created, location: @ticket }
      else
        format.html { render :new }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    respond_to do |format|
      if @ticket.update(ticket_params)
        format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.html { render :edit }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: 'Ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_ticket_message
      @ticket_message = @ticket.ticket_messages.new
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:title, :content, :priority)
    end

    def only_current_user
      unless current_user.id == @ticket.user_id
        unless current_user.admin?
          redirect_to(root_url)
        end
      end
    end
end
