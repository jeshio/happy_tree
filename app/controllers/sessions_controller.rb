class SessionsController < ApplicationController
	before_action :non_signed_in_user,  only: [:create, :new]

	def new
		
	end

	def create
		user = User.find_by_login(params[:login])
		if user && user.authenticate(params[:password])
			sign_in user
			redirect_back_or root_url
		else
			flash.now[:alert] = "Неверная комбинация логина и пароля."
			render 'new'
		end
	end

	def destroy
		sign_out
		redirect_to root_url
	end
end
