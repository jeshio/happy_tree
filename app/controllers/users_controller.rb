class UsersController < ApplicationController
  before_action :signed_in_user,      except:   [:create, :new, :check_response]
  before_action :correct_user,        only:     [:show, :edit, :update]
  before_action :admin_attribute,     only:     [:create, :update]
  before_action :non_signed_in_user,  only:     [:create, :new]
  before_action :admin_user,          only:     [:destroy, :show, :index]
  before_action :delete_himself_user, only:     :destroy
  before_action :check_confirmation,  except:   [:new, :create, :next_registration, :update, :check_response]
  before_action :for_unconfirmated,   only:     [:next_registration]
  before_action :cant_change_login,   only:     [:update]
  before_action :get_user,            only:     [:update, :edit, :invited_list]

	def index
		@users = User.paginate(page: params[:page], per_page: 15)
	end

	def show
    @user = User.find(params[:id])
	end

	def edit
	end

  def update
    params = user_params
    params = params.merge(deposit_param) if current_user.step == 1
    if @user.update_attributes(params)
      if current_user.confirmed?
        flash[:success] = "Изменения сохранены!"
        redirect_to settings_path
      else
        current_user.update_attributes({step: current_user.step + 1})

        redirect_to root_url
      end
    else
      if current_user.confirmed?
        render :edit
      else
        render :next_registration
      end  
    end
  end

  def check_response
    if !request.request_parameters['m_operation_id'].nil? &&
        !request.request_parameters['m_sign'].nil?

      id = request.request_parameters['m_orderid']

      user = User.find(id)

      # проверяем, что пользователь существует и не подтверждён
      unless user.nil? || user.confirm?
        # заплатил сколько положено
        if request.request_parameters['m_curr'] == MY_CURR &&
          request.request_parameters['m_amount'] == user.deposit
          arr = [
            request.request_parameters['m_operation_id'],
            request.request_parameters['m_operation_ps'],
            request.request_parameters['m_operation_date'],
            request.request_parameters['m_operation_pay_date'],
            request.request_parameters['m_shop'],
            request.request_parameters['m_orderid'],
            request.request_parameters['m_amount'],
            request.request_parameters['m_curr'],
            request.request_parameters['m_desc'],
            request.request_parameters['m_status'],
            SECRET_KEY
          ]
          
          if (request.request_parameters['m_sign'] == encode_for_shop(arr.join(":")) && request.request_parameters['m_status'] == 'success')
            user.update_attribute(confirmed: 1)
            @response = request.request_parameters['m_orderid'] + '|success';
          else
            @response = request.request_parameters['m_orderid'] + '|error';
          end
        end
      end
    end

    render :layout => false
  end

  def new
  	@user = User.new
  end

  def next_registration
    @user = current_user
  end

  def create
  	@user = User.new(user_params)
    @user.special_link_id = get_special_link_id unless get_special_link_id.nil?
  	if @user.save
      sign_in @user
  		redirect_to next_url
  	else
  		render 'new'
  	end
  end

  def invited_list
    @list = current_user.childrens.order('confirmed desc, created_at desc').paginate(page: params[:page], per_page: 10)
  end

  def withdraw_money
    @user = current_user
  end

  private
    def get_user
      if current_user.admin?
        @user = params[:id].nil? ? current_user : User.find(params[:id])
      else
        redirect_to root_url unless params[:id].nil?
        @user = current_user
      end
    end

  	def user_params
      params.require(:user).permit(:login, :password,
                                    :password_confirmation, :current_password,
                                    :invite_key, :email, :email_get_news, :goodness, :anonimity)
    end

    def deposit_param
      params.require(:user).permit(:deposit)
    end

    def cant_change_login
      params[:user][:login] = nil unless params[:user][:login].blank?
    end

    def delete_himself_user
      redirect_to(root_url) if User.find(params[:id]) == current_user
    end

    def admin_attribute
        params[:user][:admin] = nil unless params[:user][:admin].blank?
    end

end