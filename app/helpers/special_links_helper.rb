module SpecialLinksHelper

	def get_special_link(special_link, user_hash)
		"#{root_url}l/#{special_link}/#{user_hash}"	
	end

	def set_special_link_id(id)
		cookies.permanent[:special_link_id] = id unless signed_in?
	end

	def get_special_link_id
		cookies[:special_link_id]
	end
end
