module TicketsHelper
	def get_ticket_status_message(status)
		case status
				when 0
					"тикет закрыт"
				when 1
					"ожидается ответ пользователя"
				when 2
					"ожидается ответ администратора"
				else
					"неизвестно"
				end
	end

	def get_ticket_priority_message(priotiry)
		case priotiry
			when 0
				"низкий"
			when 1
				"средний"
			when 2
				"высокий"
			else
				"неизвестно"
		end
	end
end
