module UsersHelper

    def correct_user
      unless params[:id].nil?
        unless current_user.admin?
          @user = User.find(params[:id])
          redirect_to(root_url) unless current_user?(@user)
        end
      end
    end

    def set_inviter_hash(user_hash)
      user = User.find_by_user_hash(user_hash)

      unless signed_in?
        cookies.permanent[:inviter_hash] = user_hash unless user.nil?
      end
    end

    def get_inviter_hash
      cookies[:inviter_hash]
    end

    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

    def non_signed_in_user
      if signed_in?
        redirect_to root_url
      end
    end

    def check_confirmation
      redirect_to(next_url) unless current_user.confirmed?
    end

    def for_unconfirmated
      redirect_to(root_url) if current_user.confirmed?
    end

end
