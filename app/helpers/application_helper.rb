module ApplicationHelper
	def articlate(article, length, link, omission = '..... читать дальше..')
	  output = h truncate(article, length: length, omission: '', separator: ' ')
	  output += link_to(omission, link) if article.size > length
	  output.html_safe
	end
	
	def encode_for_shop(string)
		Digest::SHA2.new.update(string).hexdigest.upcase
	end
end
