module StaticPagesHelper
	def hide_sidebar_for_nonsigned
		@hide_sidebar = true unless confirmed?
  	end

  	def always_hide_sidebar
		@hide_sidebar = true
  	end
end
