json.array!(@special_links) do |special_link|
  json.extract! special_link, :id, :name, :special_link, :icon
  json.url special_link_url(special_link, format: :json)
end
