json.array!(@funds) do |fund|
  json.extract! fund, :id, :name, :content, :ewallet, :response, :result, :finished, :begin_time, :end_time
  json.url fund_url(fund, format: :json)
end
