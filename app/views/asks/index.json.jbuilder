json.array!(@asks) do |ask|
  json.extract! ask, :id, :content, :response, :priority, :ask_status
  json.url ask_url(ask, format: :json)
end
