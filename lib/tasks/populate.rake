# lib/tasks/populate.rake
#
# Rake task to populate development database with test data
# Run it with "rake db:populate"

namespace :db do
  desc "Erase and fill database"
  task :populate => :environment do
    require 'faker'

    Rake::Task['db:reset'].invoke

    ActiveRecord::Base.skip_callbacks = true

    admin = User.create(
      login:                  "jeshio",
      email:                  "jeshio@yandex.ru",
      password:               "password",
      password_confirmation:  "password",
      confirmed:              true)
    admin.update_attribute(:created_at, 1.year.ago)
    
    date = 8.day.ago
    12.times do |c|
      begin_time = date
      end_time = date + (rand(1..7)).days
      date = date - 8.days
      f = Fund.create(
        name:         Faker::Company.name,
        content:      Faker::Lorem.paragraph(rand(3..12)),
        begin_time:   begin_time,
        end_time:     end_time,
        ewallet:      Faker::Business.credit_card_number
        )
      f.update_attribute(:created_at, begin_time)
    end

    f = Fund.create(
      name:         Faker::Company.name,
      content:      Faker::Lorem.paragraph(rand(3..12)),
      begin_time:   1.days.ago,
      end_time:     Date.today + 7.days,
      ewallet:      Faker::Business.credit_card_number
      )
    f.update_attribute(:created_at, 1.days.ago)

    date = Date.today + 8.days
    12.times do |c|
      begin_time = date
      end_time = date + (rand(7) + 1).days
      date = date + 8.days
      f = Fund.create(
        name:         Faker::Company.name,
        content:      Faker::Lorem.paragraph(3..12),
        begin_time:   begin_time,
        end_time:     end_time,
        ewallet:      Faker::Business.credit_card_number
        )
      f.update_attribute(:created_at, begin_time)
    end

    def confirm_users(users)
      users.each do |user|
        user.step = 3
        user.confirmed = true
        user.save!(validate: false)
      end
    end

    newUsers = Array.new
    7.times do |i|
      passw = Faker::Internet.password(6, 60)
      newUsers.push(User.create(
              login:                  Faker::Internet.user_name,
              email:                  Faker::Internet.email,
              password:               passw,
              password_confirmation:  passw,
              step:                   2,
              deposit:                100))
      newUsers.last.update_attribute(:created_at, rand(1..200).days.ago)
    end

    confirm_users newUsers

    3.times do |i|
      passw = Faker::Internet.password(6, 60)
      u = User.create(
        login:                  Faker::Internet.user_name,
        email:                  Faker::Internet.email,
        password:               passw,
        password_confirmation:  passw,
        step:                   rand(1..2)
        )
      u.update_attribute(:created_at, rand(1.200).days.ago)
    end

    users = User.where(confirmed: true).to_a

    # приглашённые

    newUsers = Array.new
    rand(30..40).times do |i|
      passw = Faker::Internet.password(6, 60)
      newUsers.push(User.create(
        login:                  Faker::Internet.user_name,
        email:                  Faker::Internet.email,
        password:               passw,
        password_confirmation:  passw,
        invite_key:             users.sample.user_hash,
        step:                   2,
        deposit:                100))
      newUsers.last.update_attribute(:created_at, rand(1..200).days.ago)
    end

    confirm_users newUsers

    rand(100..150).times do |i|
      newUsers = Array.new
      users = User.where(confirmed: true)
      passw = Faker::Internet.password(6, 60)
      newUsers.push(User.create(
        login:                  Faker::Lorem.characters(rand(5..15)),
        email:                  Faker::Internet.email,
        password:               passw,
        password_confirmation:  passw,
        invite_key:             users.sample.user_hash,
        step:                   2,
        deposit:                100))
      newUsers.last.update_attribute(:created_at, rand(1..100).days.ago)
      confirm_users newUsers
    end

    rand(15..20).times do |i|
      passw = Faker::Internet.password(6, 60)
      u = User.create(
        login:                  Faker::Internet.user_name,
        email:                  Faker::Internet.email,
        password:               passw,
        password_confirmation:  passw,
        invite_key:             users.sample.user_hash,
        step:                   rand(1..2)
        )
      u.update_attribute(:created_at, rand(1.200).days.ago)
    end

    rand(35..50).times do |i|
      t = Ticket.create(
        title:      Faker::Lorem.sentence(3),
        content:    Faker::Lorem.paragraph(rand(2..14)),
        user_id:    User.where(admin: false).to_a.sample,
        priority:   rand(1..3),
        ticket_status: rand(0..2)
        )
      t.update_attribute(:created_at, rand(1.100).days.ago)
    end

    rand(12..15).times do |i|
      a = Ask.create(
        content:    Faker::Lorem.sentence(rand(5..25)),
        response:   Faker::Lorem.sentence(rand(5..25)),
        priority:   rand(1..5),
        ask_status: rand(0..1)
        )
      a.update_attribute(:created_at, rand(1..100).days.ago)
    end

    rand(12..15).times do |i|
      l = SpecialLink.create(
        name:             Faker::Internet.user_name,
        special_link:     Faker::Lorem.characters(rand(5..12))
        )
      l.update_attribute(:created_at, rand(1..100).days.ago)
    end

    rand(22..25).times do |i|
      l = FeedItem.create(
        title:              Faker::Lorem.sentence(3),
        content:            Faker::Lorem.paragraph(rand(3..12)),
        ready:              true
        )
      l.update_attribute(:created_at, rand(1..100).days.ago)
    end

    rand(12..15).times do |i|
      l = FeedItem.create(
        title:              Faker::Lorem.sentence(3),
        content:            Faker::Lorem.paragraph(rand(3..12))
        )
      l.update_attribute(:created_at, rand(1..100).days.ago)
    end
  end
end
