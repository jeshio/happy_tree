def sign_in(user, options = {})
	if options[:no_capybara]
		# Sign in when not using Capybara
		remember_token = User.new_remember_token
		cookies[:remember_token] = remember_token
		user.update_attribute(:remember_token, User.encrypt(remember_token))
	else
		visit signin_path
		fill_in "Login", 	with: user.login.upcase
		fill_in "Password", with: user.password
		click_button "войти"
	end
end

def registration(user)
	visit signup_path
	fill_in I18n.t("activerecord.attributes.user.login"),					with: user.login
	fill_in I18n.t("activerecord.attributes.user.password"),				with: user.password
	fill_in I18n.t("activerecord.attributes.user.password_confirmation"),	with: user.password
	click_button "далее"
end

def sign_out
	visit root_path
	click_link "Выход"
end

RSpec::Matchers.define :have_error_message do |message|
  match do |page|
    expect(page).to have_selector('div.alert.alert-box', text: message)
  end
end

RSpec::Matchers.define :have_success_message do |message|
	match do |page|
		expect(page).to have_selector('div.alert.alert-box', text: message)
	end
end