require "rails_helper"

RSpec.describe SpecialLinksController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/special_links").to route_to("special_links#index")
    end

    it "routes to #new" do
      expect(:get => "/special_links/new").to route_to("special_links#new")
    end

    it "routes to #show" do
      expect(:get => "/special_links/1").to route_to("special_links#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/special_links/1/edit").to route_to("special_links#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/special_links").to route_to("special_links#create")
    end

    it "routes to #update" do
      expect(:put => "/special_links/1").to route_to("special_links#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/special_links/1").to route_to("special_links#destroy", :id => "1")
    end

  end
end
