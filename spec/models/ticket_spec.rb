require 'rails_helper'

RSpec.describe Ticket, :type => :model do
  let(:user) { FactoryGirl.create(:confirmed_user) }
  
  before { @ticket = Ticket.new(title: "Test ticket", content: "all good" * 3, priority: 1, user_id: user.id) }

  subject { @ticket }

  it { should be_valid }

  describe "после сохранения" do
    before { @ticket.save }

    it { expect(@ticket.ticket_status).not_to be_blank }
    it { expect(@ticket.priority).not_to be_blank }

  end

end
