require 'rails_helper'

RSpec.describe User, :type => :model do
  ADMIN_PC  = 0.10
  INVITER_PC  = 0.50
  INVITER_2_PC  = 0.10
  INVITER_3_PC  = 0.10
  INVITER_4_PC  = 0.10

  INVITE_BONUS = 0.5 # дополнительное вознаграждение от части админа за регистрацию по приглашению

  let(:user) { FactoryGirl.create(:confirmed_user) }
  let(:admin) { FactoryGirl.create(:admin) }
  
  before { @user = User.new(login: "jeshio",
  	password: "ujif12", password_confirmation: "ujif12") }

  subject { @user }

  it { should respond_to(:login) }
  it { should respond_to(:email) }
  it { should respond_to(:email_get_news) }
  it { should respond_to(:password_digest) }
  it { should respond_to(:remember_token) }
  it { should respond_to(:inviter_id) }
  it { should respond_to(:anonimity) }
  it { should respond_to(:goodness) }
  it { should respond_to(:capital) }
  it { should respond_to(:deposit) }
  it { should respond_to(:language) }
  it { should respond_to(:confirmed) }
  it { should respond_to(:authenticate) }
  it { should respond_to(:admin) }
  it { should respond_to(:user_hash) }
  it { should respond_to(:special_link_id) }
	it { should respond_to(:step) }

  it { should be_valid }

  describe "первый пользователь - админ" do
    before { @user.save }
    specify { expect(@user.admin).to eq true }

    describe "второй пользователь уже не админ" do
      before { user.save }

      specify { expect(user.admin).to be_nil }
    end
  end

  describe "remember token" do
    before { @user.save }
    it(:remember_token) { should_not be_blank }
  end

  describe "возвращаемое методом authenticate значение" do
	  before { @user.save }
	  let(:found_user) { User.find_by(login: @user.login) }

	  describe "с валидным паролем" do
	    it { should eq found_user.authenticate(@user.password) }
	  end

	  describe "с невалидным паролем" do
	    let(:user_for_invalid_password) { found_user.authenticate("invalid") }

	    it { should_not eq user_for_invalid_password }

	    specify { expect(user_for_invalid_password).to be_falsey }
	  end
	end

  describe "проверка генерирования заполнения поля hash" do
    before do
      @user.save
      @user.reload
    end

    subject { @user.user_hash }

    it { should_not be_blank }
  end

  describe "проверка перевода email в нижний регистр перед записью" do
    before do
    	@user.email = "jeshio@yandex.ru"
  		@user.email.upcase!
  		@user.save
  		@user.reload
  	end
  	subject { @user.email }

  	it { should eq @user.email.downcase }
  end

  describe "приглашение пользователя" do
    before do
      admin.save
      user.save
    end
    
    describe "валидное" do
      before do
        @reg_user = FactoryGirl.create(:confirmed_user, invite_key: user.user_hash)
      end

      it(@reg_user) { should be_valid }

      describe "пользователь создан" do
        before { @reg_user.save }

        it "новый пользователь, приглашённый другим юзером" do
          expect(@reg_user.inviter_id).to eq user.id
        end
      end
    end

    describe "невалидное" do
      it "приглашение" do
        expect(FactoryGirl.build(:user, invite_key: 1234)).not_to be_valid
      end
    end

  end

  describe "когда email не валиден" do
  	it "не должен пройти" do
	  	addresses = %w[useR@foo,com usee@f+om.com res.s_ndd.com good@mail.c-r oo@bar..com]
	  	addresses.each do |invalid_mail|
	  		@user.email = invalid_mail
	  		expect(@user).not_to be_valid
  		end
  	end
  end

  describe "когда email валиден" do
  	it "должен пройти" do
	  	addresses = %w[user@foo.com us_e+e@fom.com res.s@ndd.ru gOOd@Ma-il.moBy]
	  	addresses.each do |valid_mail|
	  		@user.email = valid_mail
	  		expect(@user).to be_valid
  		end
  	end
  end

  describe "система накопления" do
    before { User.delete_all }
    let(:ifund) {
      ifund = FactoryGirl.build(:ifund)
      ifund.save(validate: false)
      ifund
    }
    
    let(:user1) { FactoryGirl.create(:user, deposit: 100, created_at: 100.days.ago) }
    let(:user2) { FactoryGirl.create(:user, inviter_id: user1.id, deposit: 100, created_at: 100.days.ago) }
    let(:user3_1) { FactoryGirl.create(:user, inviter_id: user2.id, deposit: 100, created_at: 100.days.ago) }
    let(:user3) { FactoryGirl.create(:user, inviter_id: user2.id, deposit: 100, created_at: 3.days.ago) }
    let(:user4) { FactoryGirl.create(:user, inviter_id: user3.id, deposit: 100, goodness: 1, created_at: 100.days.ago) }
    let(:user5) { FactoryGirl.create(:user, inviter_id: user4.id, deposit: 100, created_at: 100.days.ago) }
    let(:user6) { FactoryGirl.create(:user, inviter_id: user5.id, deposit: 100, created_at: 100.days.ago) }
    let(:user7) { FactoryGirl.create(:user, inviter_id: user5.id, deposit: 100, created_at: 6.days.ago) }
    let(:user8) { FactoryGirl.create(:user, inviter_id: user7.id, deposit: 100, created_at: 100.days.ago) }
    let(:user9) { FactoryGirl.create(:user, inviter_id: user8.id, deposit: 100, created_at: 100.days.ago) }
    let(:user10) { FactoryGirl.create(:user, inviter_id: user9.id, deposit: 100, created_at: 2.days.ago) }
    let(:user11) { FactoryGirl.create(:user, inviter_id: user10.id, deposit: 100, created_at: 100.days.ago) }
    let(:user12) { FactoryGirl.create(:user, inviter_id: user10.id, deposit: 100, created_at: 100.days.ago) }

    before do
      ifund.save
      admin.save
      user1.save
      user2.save
      user3.save
      user3_1.save
      user4.save
      user5.save
      user6.save
      user7.save
      user8.save
      user9.save
      user10.save
      user11.save
      user12.save
      User.all.each do |u|
        u.confirmed = true
        u.deposit = 100
        u.save!
      end
      user1.reload
      user2.reload
      user3.reload
      user3_1.reload
      user4.reload
      user5.reload
      user6.reload
      user7.reload
      user8.reload
      user9.reload
      user10.reload
      user11.reload
      user12.reload
    end

    g_capital = 0

    it "пользователь с неуказанным inviter_id должен следовать за первым пользователем" do
      expect(user1.inviter_id).to eq User.first.id
    end

    it "первый пользователь должен получить процент с каждого вклада и за приглашённых" do
      capital = 0
      User.where(id: User.second.id..User.last.id).each do |u|
        parents = u.parents_count
        
        goodnesses = u.get_goodnesses

        unless goodnesses.length > 0 || u.goodness # исключаем ветки Goodness'ов

          if u.has_admin?

            case parents
            when 1
              capital += u.deposit * INVITER_PC
            when 2
              capital += u.deposit * INVITER_2_PC
            when 3
              capital += u.deposit * INVITER_3_PC
            when 4
              capital += u.deposit * INVITER_4_PC
            end

          else

            capital += u.deposit * ADMIN_PC

          end
          if parents != 1
            capital -= u.deposit * ADMIN_PC * INVITE_BONUS if !u.inviter.admin? && u.inviter.created_at > 7.days.ago
          end
        end
      end
      g_capital += capital

      expect(User.first.capital).to eq capital
    end

    it "фонд должен получить остаток с каждого вклада" do
      capital = 0

      User.where(id: User.second.id..User.last.id).each do |u|
        parents = u.parents_count
        
        goodnesses = u.get_goodnesses
        
        increament = u.deposit

        # исключая случаи, где есть Goodness (без вычета админу и самому Goodness)
        increament -= u.deposit * INVITER_PC if parents > 0 && !goodnesses.include?(0) && !(!goodnesses.empty? && u.get_parent(0).admin?)
        
        increament -= u.deposit * INVITER_2_PC if parents > 1 && !goodnesses.include?(1) && !(!goodnesses.empty? && u.get_parent(1).admin?)

        increament -= u.deposit * INVITER_3_PC if parents > 2 && !goodnesses.include?(2) && !(!goodnesses.empty? && u.get_parent(2).admin?)

        increament -= u.deposit * INVITER_4_PC if parents > 3 && !goodnesses.include?(3) && !(!goodnesses.empty? && u.get_parent(3).admin?)

        unless u.has_admin? || !goodnesses.empty? || u.goodness
          increament -= u.deposit * ADMIN_PC
        else
          increament -= u.deposit * ADMIN_PC * INVITE_BONUS if !u.inviter.admin? && u.inviter.created_at > 7.days.ago
        end

        capital += increament
      end

      g_capital += capital

      expect(Fund.first.result).to eq capital
    end

    it "второй пользователь должен получить процент с каждого вклада своей ветки" do
      capital = user2.deposit * INVITER_PC +
        user3.deposit * INVITER_2_PC +
        user3_1.deposit * INVITER_2_PC +
        user4.deposit * INVITER_3_PC +
        user5.deposit * INVITER_4_PC

      g_capital += capital

      expect(user1.capital).to eq capital
    end

    it "третий пользователь должен получить процент с каждого вклада своей ветки" do
      capital = user3.deposit * INVITER_PC +
        user3_1.deposit * INVITER_PC +
        user4.deposit * INVITER_2_PC +
        user5.deposit * INVITER_3_PC +
        user6.deposit * INVITER_4_PC +
        user7.deposit * INVITER_4_PC

      g_capital += capital

      expect(user2.capital).to eq capital
    end

    it "четвёртый пользователь должен получить процент с каждого вклада своей ветки" do
      capital = user4.deposit * INVITER_PC +
        user5.deposit * INVITER_2_PC +
        user6.deposit * INVITER_3_PC +
        user7.deposit * INVITER_3_PC +
        user8.deposit * INVITER_4_PC

      # бонус от админа
      capital += user4.deposit * ADMIN_PC * INVITE_BONUS

      g_capital += capital

      expect(user3.capital).to eq capital
    end

    it "пятый пользователь не должен получить процент с каждого вклада своей ветки, он Goodness" do
      capital = 0

      expect(user4.capital).to eq capital
    end

    it "шестой пользователь должен получить процент с каждого вклада своей ветки" do
      capital = user6.deposit * INVITER_PC +
        user7.deposit * INVITER_PC +
        user8.deposit * INVITER_2_PC +
        user9.deposit * INVITER_3_PC +
        user10.deposit * INVITER_4_PC

      g_capital += capital

      expect(user5.capital).to eq capital
    end

    it "восьмой пользователь должен получить процент с каждого вклада своей ветки" do
      capital = user8.deposit * INVITER_PC +
        user9.deposit * INVITER_2_PC +
        user10.deposit * INVITER_3_PC +
        user11.deposit * INVITER_4_PC +
        user12.deposit * INVITER_4_PC

      # бонус от админа
      capital += user8.deposit * ADMIN_PC * INVITE_BONUS

      g_capital += capital

      expect(user7.capital).to eq capital
    end

    it "девятый пользователь должен получить процент с каждого вклада своей ветки" do
      capital = user9.deposit * INVITER_PC +
        user10.deposit * INVITER_2_PC +
        user11.deposit * INVITER_3_PC +
        user12.deposit * INVITER_3_PC

      g_capital += capital

      expect(user8.capital).to eq capital
    end

    it "десятый пользователь должен получить процент с каждого вклада своей ветки" do
      capital = user10.deposit * INVITER_PC +
        user11.deposit * INVITER_2_PC +
        user12.deposit * INVITER_2_PC

      g_capital += capital

      expect(user9.capital).to eq capital
    end

    it "одинадцатый пользователь должен получить процент с каждого вклада своей ветки" do
      capital = user11.deposit * INVITER_PC +
        user12.deposit * INVITER_PC

      # дополнительно часть администратора за регистрацию по приглашению менее 7 дней назад
      capital += user11.deposit * ADMIN_PC * INVITE_BONUS +
        user12.deposit * ADMIN_PC * INVITE_BONUS

      g_capital += capital

      expect(user10.capital).to eq capital
    end

    it { expect(user6.capital).to eq 0 }
        
    it { expect(user11.capital).to eq 0 }

    it { expect(user12.capital).to eq 0 }

    it { expect(user3_1.capital).to eq 0 }

    it { expect(g_capital).to eq User.where(id: User.second.id..User.last.id).sum(:deposit) }

    describe "изменение настроек не должно повлиять на капитал" do
      before do
        user1.anonimity = true
        user1.save
      end

      it { expect(user1.capital).to eq user1.reload.capital }
      it { expect(user2.capital).to eq user2.reload.capital }
    end

  end

end
