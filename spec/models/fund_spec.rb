require 'rails_helper'

RSpec.describe Fund, :type => :model do
  let!(:ifund) { FactoryGirl.create(:ifund) }
  let(:admin) { FactoryGirl.create(:admin) }
	let(:future_fund) { FactoryGirl.build(:ifund, :future) }
  
  before { @fund = Fund.new(name: future_fund.name, content: future_fund.content,
  	begin_time: future_fund.begin_time, end_time: future_fund.end_time) }

  subject { @fund }

  it { should respond_to(:name) }
  it { should respond_to(:content) }
  it { should respond_to(:result) }
  it { should respond_to(:finished) }
  it { should respond_to(:begin_time) }
  it { should respond_to(:end_time) }
  it { should respond_to(:response) }

  it { should be_valid }

  describe "параметры поумолчанию после записи" do
  	before do
  		@fund.save
  		@fund.reload
  	end

  	it { expect(@fund.result).to eq(0) }
  	
  	it { expect(@fund.finished).to eq(false) }
  	
  	it { expect(@fund.response).to be_blank }

  end

  describe "добавление невалидного фонда" do
  	
  	describe "дата начала позже, чем дата окончания" do
  		before do
  			@fund.begin_time = Date.today + 11
  			@fund.end_time = Date.today + 5
  		end

  		it { should_not be_valid }
  	end

  	describe "интервал времени пересекается с другими фондами" do
  		before do
  			ifund.save
  			@fund.begin_time = ifund.end_time - 1
  			@fund.end_time = ifund.end_time + 3
  		end

  		it { should_not be_valid }
  	end

  	describe "дата начала раньше, чем текущая дата" do
  		before do
  			@fund.begin_time = Date.today - 2
  			@fund.end_time = Date.today  + 3
  		end

  		it { should_not be_valid }
  	end
  end

  describe "добавление валидного фонда" do
  	describe "дата начала с текущей датой" do
  		before do
  			@fund.begin_time = Date.today
  			@fund.end_time = Date.today  + 5
  		end

  		it { should be_valid }
  	end
  end
end
