FactoryGirl.define do
	factory :feed_item do
		sequence(:title)  	{ |n| "The News #{n}" }
	    
	    content	"a" * 500

	    ready false
	    
	    trait :prepared do
	    	ready true
	    end
	end
end