FactoryGirl.define do
    factory :ticket do
		sequence(:title)  { |n| "title#{n}" }
	    
	    content "c" * 1500

	    user_id 1

	    priority 3

	    ticket_status 2
	end
end