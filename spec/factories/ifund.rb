FactoryGirl.define do
	factory :ifund, class: :fund do
		sequence(:name)  	{ |n| "fund#{n}" }
	    
	    content 			"a" * 500
	    
	    ewallet 			"R123AB987654"

	    begin_time 			Date.today + 1

		end_time			Date.today + 5

		after(:create) do |fund|
		    fund.begin_time = 2.day.ago
	  	end

	    trait :old do
	    	response 			"b" * 400
	    
	    	result 				435.5

			after(:create) do |fund|
			    fund.begin_time 		11.day.ago

				fund.end_time		2.day.ago
		  	end

			finished true
	    end

	    trait :future do
	    	after(:create) do |fund|
		    	fund.begin_time 	Date.today + 5

				fund.end_time		Date.today + 12
			end
    	end
	end
end