FactoryGirl.define do
    factory :user do
		sequence(:login)  { |n| "user#{n}" }
	    password "foobar"
	    password_confirmation "foobar"
	    sequence(:email) { |n| "test-#{n}@yandex.ru" }
	    sequence(:phoneNumber) { |n| "7900123456#{n}" }

	    factory :anonym do
	    	anonimity true
	    	confirmed true
	    end

	    factory :goodman do
	    	goodness true
	    	confirmed true
	    end
	    
	    factory :admin do
	    	sequence(:email) { |n| "jeshio-#{n}@yandex.ru" }
	    	confirmed true
	    	admin true
	    end

	    factory :confirmed_user do
	    	step 3
		    confirmed true
		    deposit 100
	    end

	    factory :fund do
	    	confirmed true
	    	fund true
	    end
	end
end