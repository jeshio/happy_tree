FactoryGirl.define do
    factory :special_link do
		sequence(:name) { |n| "special_link#{n}" }
		sequence(:special_link) { |n| "link#{n}" }
	end
end