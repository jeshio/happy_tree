require 'rails_helper'

RSpec.describe "Funds", :type => :request do
	let!(:ifund) { FactoryGirl.create(:ifund, name: "TestedFund")}
  let(:admin) { FactoryGirl.create(:admin) }
  let(:user) { FactoryGirl.create(:user) }

	subject { page }

  before do
    3.times { |i| FactoryGirl.build(:ifund, :old).save }
      
    ifund.save
    ifund.reload

    3.times { |i| FactoryGirl.build(:ifund, :future).save }
  end


  describe "текущий фонд" do
    before do
    	visit funds_current_path
    end

    it { should have_content(Fund.first.name) }
  end

  describe "страница 'завершение активности фонда'" do
    before do
      sign_in admin
      visit finish_fund_path(ifund)
    end

    it { should have_title("звершить активность фонда") }

    it "фонд должен быть не подтверждён" do
      expect(Fund.find_by_name(ifund.name).finished).to be_falsey
    end

    describe "заполненая форма" do
      let(:response) { "Пустой ответ от фонда." } 
      before do
        fill_in "Ответ",                      with: response
        fill_in "Перечислено дополнительно",  with: 100

        click_button "Сохранить"
      end

      it { should_not have_content("error") }
      
      it "фонд должен быть подтверждён" do
        expect(Fund.find_by_name(ifund.name).finished).to be_truthy
      end

      it "у фонда должен быть ответ" do
        expect(Fund.find_by_name(ifund.name).response).to eq response
      end

      it "у фонда должен быть дополнительный зарабаток" do
        expect(Fund.find_by_name(ifund.name).result_other).to eq 100
      end
    end

  end

  describe "страница со всеми фондами, которым помогли" do
    before do
      sign_in user
      visit funds_finished_path
    end

    it { should have_title("помогли") }

    it { should have_content(Fund.finished.first()) }

    it { should_not have_content(Fund.non_finished.first()) }

  end

  describe "страница 'добавление фонда'" do
  	before do
  		sign_in admin
  		visit new_fund_path
      Fund.delete_all
  	end

  	it { should have_title("добавить фонд") }

  	it { should have_button("Сохранить") }

  	describe "валидное добавление" do
  		before do
  			fill_in "Name",					with: ifund.name
  			fill_in "Content",			with: ifund.content
  			fill_in "Ewallet",			with: ifund.ewallet

        begin_time  = ifund.end_time + 12
        end_time    = ifund.end_time + 20

  			select begin_time.year,											from: 'fund_begin_time_1i'
  			select I18n.t("date.month_names")[begin_time.month],	from: 'fund_begin_time_2i'
  			select begin_time.day,	    								from: 'fund_begin_time_3i'
  			
  			select end_time.year,												from: 'fund_end_time_1i'
  			select I18n.t("date.month_names")[end_time.month],		from: 'fund_end_time_2i'
  			select end_time.day,				     						from: 'fund_end_time_3i'

  			click_button "Сохранить"
  		end

      it { should_not have_content("error") }
      
      it "должен быть добавлен фонд" do
        expect(Fund.find_by_name(ifund.name)).not_to be_falsey
      end

      after { Fund.delete_all }
  	end

    it "фонд не должен быть добавлен" do
      expect { click_button "Сохранить" }.not_to change(Fund, :count)
    end
  end

end
