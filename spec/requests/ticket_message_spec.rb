require 'rails_helper'

RSpec.describe "TicketMessages", :type => :request do
  let(:closed) { 0 }
  let(:user_wait) { 1 }
  let(:admin_wait) { 2 }

	let(:user) { FactoryGirl.create(:confirmed_user) }
	let(:user2) { FactoryGirl.create(:confirmed_user) }
	let(:admin) { FactoryGirl.create(:admin) }

  before do
  		admin.save
		2.times { |i| FactoryGirl.create(:ticket, user_id: user.id, ticket_status: closed).save }
		2.times { |i| FactoryGirl.create(:ticket, user_id: user.id, priority: 1).save }
		1.times { |i| FactoryGirl.create(:ticket, user_id: user.id, ticket_status: user_wait).save }
		3.times { |i| FactoryGirl.create(:ticket, user_id: user2.id).save }
	end

	let(:closed_ticket) { user.tickets.first }
	let(:open_ticket_to_admin) { user.tickets.third }
	let(:open_ticket_to_user) { user.tickets.fifth }

	subject { page }

	describe "создание сообщения" do

		describe "пользователем" do
			before do
				sign_in user
			end

			describe "для открытого тикета (ожидается ответ администратора)" do
				before do
					visit ticket_path(open_ticket_to_admin)
					fill_in :ticket_message_content, with: "as " * 7
					click_button "Отправить"
					open_ticket_to_admin.reload
				end

				it { should have_content("as " * 7) }
				it { should_not have_content("error") }
				it { should have_content(open_ticket_to_admin.title) }
				it { expect(open_ticket_to_admin.ticket_status).to eq admin_wait }

			end

			describe "для открытого тикета (ожидается ответ пользователя)" do
				before do
					visit ticket_path(open_ticket_to_user)
					fill_in :ticket_message_content, with: "av " * 7
					click_button "Отправить"
					open_ticket_to_user.reload
				end

				it { should have_content("av " * 7) }
				it { should_not have_content("error") }
				it { should have_content(open_ticket_to_user.title) }
				it { expect(open_ticket_to_user.ticket_status).to eq admin_wait }

			end

			describe "для закрытого тикета" do
				before do
					visit ticket_path(closed_ticket)
				end

				it { should_not have_button("Отправить") }

			end

		end

		describe "администратором" do
			before do
				sign_in admin
			end

			describe "для открытого тикета (ожидается ответ администратора)" do
				before do
					visit ticket_path(open_ticket_to_admin)
					fill_in :ticket_message_content, with: "as " * 7
					click_button "Отправить"
					open_ticket_to_admin.reload
				end

				it { should have_content("as " * 7) }
				it { should_not have_content("error") }
				it { should have_content(open_ticket_to_admin.title) }
				it { expect(open_ticket_to_admin.ticket_status).to eq user_wait }

			end

			describe "для открытого тикета (ожидается ответ пользователя)" do
				before do
					visit ticket_path(open_ticket_to_user)
					fill_in :ticket_message_content, with: "av " * 7
					click_button "Отправить"
					open_ticket_to_user.reload
				end

				it { should have_content("av " * 7) }
				it { should_not have_content("error") }
				it { should have_content(open_ticket_to_user.title) }
				it { expect(open_ticket_to_user.ticket_status).to eq user_wait }

			end

			describe "для закрытого тикета" do
				before do
					visit ticket_path(closed_ticket)
				end

				it { should_not have_button("Отправить") }

			end
		end
	end

	describe "удаление сообщения" do

		before do
			3.times { |i| FactoryGirl.create(:ticket_message, user_id: user.id, ticket_id: open_ticket_to_admin.id).save }
			3.times { |i| FactoryGirl.create(:ticket_message, user_id: user.id, ticket_id: open_ticket_to_user.id).save }
			3.times { |i| FactoryGirl.create(:ticket_message, user_id: user.id, ticket_id: closed_ticket.id).save }
			3.times { |i| FactoryGirl.create(:ticket_message, user_id: admin.id, ticket_id: open_ticket_to_admin.id).save }
			3.times { |i| FactoryGirl.create(:ticket_message, user_id: admin.id, ticket_id: open_ticket_to_user.id).save }
			3.times { |i| FactoryGirl.create(:ticket_message, user_id: admin.id, ticket_id: closed_ticket.id).save }
		end

		describe "администратором" do
			before do
				sign_in admin
			end

			describe "для открытого тикета (ожидается ответ администратора)" do
				before do
					visit ticket_path(open_ticket_to_admin)
					click_link("delete_#{open_ticket_to_admin.ticket_messages.second.id}")
					click_link("delete_#{open_ticket_to_admin.ticket_messages.fourth.id}")
					open_ticket_to_admin.reload
				end

				it { should_not have_content("error") }
				it { should have_content(open_ticket_to_admin.title) }
				it { expect(open_ticket_to_admin.ticket_messages.count).to eq 4 }

			end

			describe "для открытого тикета (ожидается ответ пользователя)" do
				before do
					visit ticket_path(open_ticket_to_user)
					click_link("delete_#{open_ticket_to_user.ticket_messages.third.id}")
					click_link("delete_#{open_ticket_to_user.ticket_messages.fourth.id}")
					open_ticket_to_user.reload
				end

				it { should_not have_content("error") }
				it { should have_content(open_ticket_to_user.title) }
				it { expect(open_ticket_to_user.ticket_messages.count).to eq 4 }

			end

			describe "для закрытого тикета" do
				before do
					visit ticket_path(closed_ticket)
				end

				it { should_not have_link("delete_#{closed_ticket.ticket_messages.first.id}") }

			end

		end
	end
end