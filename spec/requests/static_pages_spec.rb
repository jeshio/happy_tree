require 'rails_helper'

RSpec.describe "StaticPages", :type => :request do
	let(:closed) { 0 }
	let(:user_wait) { 1 }
	let(:admin_wait) { 2 }

	before do
		7.times { |i| FactoryGirl.create(:user).save }
		visit root_path
	end

	let(:text_signup) { "Присоединиться!" }

	subject { page }

	it { should_not have_selector('.side-bar') }

	it { should have_link(text_signup) }

	describe "главная страница" do
		let(:user) { FactoryGirl.create(:confirmed_user) }
				
		before { visit root_path }

		it { should_not have_link("тикеты") }

		describe "для зарегестрированного пользователя" do
			before do
				FactoryGirl.create(:ticket, user_id: user.id, ticket_status: closed).save
				FactoryGirl.create(:ticket, user_id: user.id, priority: 1).save
				FactoryGirl.create(:ticket, user_id: user.id, ticket_status: user_wait).save
				sign_in user
				visit root_path
			end

			it { should have_title(user.login) }
			it { should have_link("тикеты") }

			describe "имеются тикеты" do

				describe "(закрытые)" do
					it { should_not have_content("тикет: есть ответ") }
					it { should_not have_content("открыт тикет") }
				end

				describe "(открытые)" do
					it { should have_content("ожидается ответ администратора") }
				end

				describe "(открытые с полученным ответом)" do
					it { should have_content("ожидается ответ пользователя") }
				end

			end
		end
	end

	describe "страница 'Достижения проекта'" do
		before { visit project_achievements_path }

		it { should_not have_selector('.side-bar') }
		it { should have_content('Количество присоединившихся') }
		# it { should have_content('Средний вклад') }
		it { should have_content('Всего отправлено в фонды') }
		# динамические данные
		it { should have_content(User.count) }
		it { should have_content(User.average(:deposit)) }
	end

	describe "страница 'о проекте это работает'" do
		before { visit how_it_work_path }

		# Как это работает?
		it { should_not have_selector('.side-bar') }
		it { should have_title('о проекте') }
	end

	describe "страница 'помощь'" do
		let(:user) { FactoryGirl.create(:confirmed_user) }
		before do
			sign_in user
			visit help_path
		end

		it { should have_title('помощь') }

		it { should have_link('Написать тикет') }
	end

	it "должны быть рабочие ссылки в меню" do
		visit root_path

		click_link('Достижения проекта')
		should have_title('достижения проекта')

		click_link('О проекте')
		should have_title('о проекте')
	end
end
