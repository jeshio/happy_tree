require 'rails_helper'

RSpec.describe "Tickets", :type => :request do
  let(:closed) { 0 }
  let(:user_wait) { 1 }
  let(:admin_wait) { 2 }

	let(:user) { FactoryGirl.create(:confirmed_user) }
	let(:user2) { FactoryGirl.create(:confirmed_user) }  
  let(:admin) { FactoryGirl.create(:admin) }

  before do
    admin.save
		2.times { |i| FactoryGirl.create(:ticket, user_id: user.id, ticket_status: closed).save }
		2.times { |i| FactoryGirl.create(:ticket, user_id: user.id, priority: 1).save }
		1.times { |i| FactoryGirl.create(:ticket, user_id: user.id, ticket_status: user_wait).save }
		3.times { |i| FactoryGirl.create(:ticket, user_id: user2.id).save }
	end

	let(:closed_ticket) { user.tickets.first }
	let(:open_ticket_to_admin) { user.tickets.third }
	let(:open_ticket_to_user) { user.tickets.fifth }

	subject { page }

  describe "список тикетов" do

    describe "для пользователя" do
    	before do
    		sign_in user
    		visit tickets_path
    	end

    	it "не должен содержать тикеты других пользователей" do
    		Ticket.where('user_id != ?', user.id).each do |t|
    			should_not have_content(t.title)
    		end
    	end

    	it "должен содержать тикеты этого пользователя" do
    		user.tickets.each do |t|
    			should have_content(t.title)
    		end
    	end

    end

    describe "для администратора" do
    	before do
    		sign_in admin
    		visit tickets_path
    	end

    	it "должен содержать тикеты всех пользователей" do
    		Ticket.all.each do |ticket|
    			should have_content(ticket.title)
    		end
    	end
    end
  end

  describe "просмотр тикета" do

  	describe "пользователем" do
  		before { sign_in user }

  		describe "(закрытый тикет)" do
  			before { visit ticket_path(closed_ticket) }

  			it { should have_content 	"тикет закрыт" }
  			it { should have_title 		closed_ticket.title }
  			it { should have_title 		closed_ticket.id }
  			it { should have_content 	closed_ticket.title }
  			it { should have_content 	closed_ticket.content }
  		end

  		describe "(тикет ждёт ответа администратора)" do
    		before { visit ticket_path(open_ticket_to_admin) }

  			it { should have_content 	"ожидается ответ администратора" }
  			it { should have_title 		open_ticket_to_admin.title }
  			it { should have_title 		open_ticket_to_admin.id }
  			it { should have_content 	open_ticket_to_admin.title }
  			it { should have_content 	open_ticket_to_admin.content }
    	end

    	describe "(тикет ждёт ответа пользователя)" do
    		before { visit ticket_path(open_ticket_to_user) }

  			it { should have_content 	"ожидается ответ пользователя" }
  			it { should have_title 		open_ticket_to_user.title }
  			it { should have_title 		open_ticket_to_user.id }
  			it { should have_content 	open_ticket_to_user.title }
  			it { should have_content 	open_ticket_to_user.content }
    	end

    	describe "просмотр чужого тикета" do

  			before do
  				sign_in user, no_capybara: true
	      	get ticket_path(user2.tickets.first)
	      end

	      specify { expect(response).to redirect_to(root_url) }

	    end

  	end

  	describe "администратором" do
  		before { sign_in admin }

    	describe "(закрытый тикет)" do
    		before { visit ticket_path(closed_ticket) }

  			it { should have_content 	"тикет закрыт" }
  			it { should have_title 		closed_ticket.title }
  			it { should have_title 		closed_ticket.id }
  			it { should have_content 	closed_ticket.title }
  			it { should have_content 	user.login }
  			it { should have_content 	closed_ticket.content }
  			it { should have_link 		"Есть вопрос" }
  			it { should_not have_link "Вопрос решён" }

  			describe "открытие тикета" do
  				before { click_link "Есть вопрос" }

  				it { should have_content 	"ожидается ответ пользователя" }
  			end
    	end

    	describe "(тикет ждёт ответа администратора)" do
    		before { visit ticket_path(open_ticket_to_admin) }

  			it { should have_content 	"ожидается ответ администратора" }
  			it { should have_title 		open_ticket_to_admin.title }
  			it { should have_title 		open_ticket_to_admin.id }
  			it { should have_content 	open_ticket_to_admin.title }
  			it { should have_content 	user.login }
  			it { should have_content 	open_ticket_to_admin.content }
  			it { should have_link 		"Вопрос решён" }
  			it { should_not have_link "Есть вопрос" }

  			describe "закрытие тикета" do
  				before { click_link "Вопрос решён" }

  				it { should have_content 	"тикет закрыт" }
  			end
    	end

    	describe "(тикет ждёт ответа пользователя)" do
    		before { visit ticket_path(open_ticket_to_user) }

  			it { should have_content 	"ожидается ответ пользователя" }
  			it { should have_title 		open_ticket_to_user.title }
  			it { should have_title 		open_ticket_to_user.id }
  			it { should have_content 	open_ticket_to_user.title }
  			it { should have_content 	user.login }
  			it { should have_content 	open_ticket_to_user.content }
  			it { should have_link 		"Вопрос решён" }
  			it { should_not have_link "Есть вопрос" }

  			describe "закрытие тикета" do
  				before { click_link "Вопрос решён" }

  				it { should have_content 	"тикет закрыт" }
  			end
    	end
  	end
  end

  describe "редактирование тикета" do
  			
  	describe "пользователем" do
  		before { sign_in user }

  		before do
				sign_in user, no_capybara: true
      	get edit_ticket_path(open_ticket_to_user)
      end

      specify { expect(response).to redirect_to(root_url) }
  	end
  end
end
