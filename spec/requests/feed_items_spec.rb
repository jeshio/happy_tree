require 'rails_helper'

RSpec.describe "FeedItems", :type => :request do
  subject { page }
  let(:user) { FactoryGirl.create(:confirmed_user) }
  let(:admin) { FactoryGirl.create(:admin) }

  before do
  	admin.save
  	FactoryGirl.create(:feed_item, content: "badnews"*4).save
  	2.times { FactoryGirl.create(:feed_item, :prepared).save }
  end

  describe "гость" do

    	before { visit root_url }

    	it "должны выводиться все подтверждённые новости" do
    		FeedItem.prepared.each do |item|
    			should have_content item.title
    			should have_content item.content
    		end
    	end

    	it "ни одна не подтверждённая новость не должна выводиться" do
    		FeedItem.unprepared.each do |item|
    			should_not have_content item.title
    			should_not have_content item.content
    		end
    	end

  end

  describe "вошедший администратор" do
  	before { sign_in admin }

  	describe "главная страница" do
    	before { visit root_url }

    	it "должны выводиться все подтверждённые новости" do
    		FeedItem.prepared.each do |item|
    			should have_content item.title
    			should have_content item.content
    		end
    	end

    	it "ни одна не подтверждённая новость не должна выводиться" do
    		FeedItem.unprepared.each do |item|
    			should_not have_content item.title
    			should_not have_content item.content
    		end
    	end
    end

  end

  describe "вошедший пользователь" do
  	before { sign_in user }

    describe "главная страница" do
    	before { visit root_url }

    	it "должны выводиться все подтверждённые новости" do
    		FeedItem.prepared.each do |item|
    			should have_content item.title
    			should have_content item.content
    		end
    	end

    	it "ни одна не подтверждённая новость не должна выводиться" do
    		FeedItem.unprepared.each do |item|
    			should_not have_content item.title
    			should_not have_content item.content
    		end
    	end
    end
  end
end
