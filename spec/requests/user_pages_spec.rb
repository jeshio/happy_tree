require 'rails_helper'

RSpec.describe "UserPages", :type => :request do
	subject { page }
    let(:fund) { FactoryGirl.create(:fund) }
    let(:admin) { FactoryGirl.create(:admin) }
    let(:user) { FactoryGirl.create(:confirmed_user) }

    describe "страница регистрации" do
    	before { visit signup_path }
        let(:submit) { "далее" }

    	describe "с неверной информацией" do
            it "создание невалидного юзера" do
                expect { click_button submit }.not_to change(User, :count)
            end

            describe "после отправки пустой формы" do
                before { click_button submit }

                it { should have_content("Некорректно") }
                it { should have_selector('form input') }
            end
        end

        describe "с верной информацией" do
        	before do
        		fill_in I18n.t("activerecord.attributes.user.login"),			with: "testing"
        		fill_in I18n.t("activerecord.attributes.user.password"),			with: "password"
        		fill_in I18n.t("activerecord.attributes.user.password_confirmation"),		with: "password"
                find(:css, "#user_anonimity").set(true)
        	end

        	it "должно создать пользователя" do
        		expect { click_button submit }.to change(User, :count)
        	end

            describe "создан пользователь" do
                before { click_button submit }

                it "должен быть неподтверждён" do
                    expect(User.find_by_login("testing").confirmed).to be_falsey
                end

                it "должен быть анонимным" do
                    expect(User.find_by_login("testing").anonimity).to be_truthy
                end

                it { should have_content("Дополнительная информация") }

                describe "замена депозита в 2-м шаге" do
                    before do
                        find("#user_deposit").set(99)
                        click_button submit
                    end

                    it "должен остаться на том же шаге" do
                        expect(User.find_by_login("testing").step).to eq 1
                    end
                end

                describe "ввод дополнительной информации" do
                    before do
                        fill_in I18n.t("activerecord.attributes.user.email"), with: "testing@mail.ru"
                        find(:css, "#user_email_get_news").set(true)
                        find(:css, "#user_goodness").set(true)
                        click_button "далее"
                    end

                    it "должен быть не подтверждён" do
                        expect(User.find_by_login("testing").confirmed).to be_falsey
                    end

                    it "текущий шаг должен установиться на 2" do
                        expect(User.find_by_login("testing").step).to eq 2
                    end

                    it "должен быть установлен email" do
                        expect(User.find_by_login("testing").email).to eq "testing@mail.ru"
                    end

                    it "поле получения новостей должно быть установлено" do
                        expect(User.find_by_login("testing").email_get_news).to be_truthy
                    end

                    it "поле goodness должно быть true" do
                        expect(User.find_by_login("testing").goodness).to be_truthy
                    end

                    it "должен быть установлен депозит" do
                        expect(User.find_by_login("testing").deposit).to eq 100
                    end
                end

            	describe "создание второго пользователя по приглашению неактивированного" do
            		before do
                        sign_out
            			visit signup_path
    	        		fill_in I18n.t("activerecord.attributes.user.login"),			with: "testing2"
    	        		fill_in I18n.t("activerecord.attributes.user.password"),			with: "password"
    	        		fill_in I18n.t("activerecord.attributes.user.password_confirmation"),		with: "password"
    	        		fill_in "Invite-key",		with: User.find_by_login("testing").user_hash
    	        	end


    	        	it "не должно создать пользователя" do
    	        		expect { click_button submit }.not_to change(User, :count)
    	        	end
            	end
            end
        end
    end

    describe "страница 'настройки'" do
        before do
            sign_in user
            visit settings_path
        end

        it { should have_content(user.login) }

        describe "отправка информации без изменений" do
            before { click_button "сохранить" }

            it { should have_content("сохранены") }
            it { should have_title('настройки') }

            specify { expect(user.reload.email).not_to eq "" }

        end

        describe "отправка запроса на изменения депозита" do
            let(:params) do
                { user: { deposit: 1000 } }
            end

            before do
                sign_in user, no_capybara: true
                patch settings_path, params
            end

            specify { expect(user.reload.deposit).not_to eq 1000 }
        end

        describe "отправка запроса на изменения логина" do
            let(:params) do
                { user: { login: "newlogin" } }
            end

            before do
                sign_in user, no_capybara: true
                patch settings_path, params
            end

            specify { expect(user.reload.login).not_to eq "newlogin" }
        end

        describe "отправка запроса на изменение пригласившего" do
            let(:params) do
                { user: { invite_key: fund.user_hash } }
            end

            before do
                fund.save
                sign_in user, no_capybara: true
                patch settings_path, params
            end

            specify { expect(user.reload.inviter_id).not_to eq fund.id }
        end

        describe "отправка запроса на изменения капитала" do
            let(:params) do
                { user: { capital: 500 } }
            end

            before do
                sign_in user, no_capybara: true
                patch settings_path, params
            end

            specify { expect(user.reload.capital).not_to eq 500 }
        end

        describe "изменение пароля" do
            before do
                fill_in I18n.t("activerecord.attributes.user.password"),                with: user.password + "1"
                fill_in I18n.t("activerecord.attributes.user.password_confirmation"),   with: user.password + "1"
            end

            describe "с неверным текущим" do
                before do
                    fill_in I18n.t("activerecord.attributes.user.current_password"),     with: user.password + "1"
                    click_button "сохранить"
                    user.reload
                end

                it { should_not have_content("сохранены") }
                it { should have_title('настройки') }

                it "не должна проходить авторизация с новым паролем" do
                    expect(user.authenticate(user.password + "1")).to be_falsey
                end
            end

            describe "с верным текущим" do
                before do
                    fill_in I18n.t("activerecord.attributes.user.current_password"),     with: user.password
                    click_button "сохранить"
                    user.reload
                end

                it { should have_content("сохранены") }
                it { should have_title('настройки') }

                it "должна проходить авторизация с новым паролем" do
                    expect(user.authenticate(user.password + "1")).to be_truthy
                end
            end
        end

        describe "установить E-mail" do
            describe "не валидно" do
                before do
                    user.email = "valid@mail.com"
                    fill_in I18n.t("activerecord.attributes.user.email"),     with: ""
                    click_button "сохранить"
                    user.reload
                end

                it { should have_title('настройки') }
                
                specify { expect(user.email).not_to eq "" }
            end

            describe "валидно" do
                before do
                    fill_in I18n.t("activerecord.attributes.user.email"),     with: "new@mail.com"
                    click_button "сохранить"
                    user.reload
                end

                it { should have_content("сохранены") }
                it { should have_title('настройки') }
                
                specify { expect(user.email).to eq "new@mail.com" }
            end
        end
    end

    describe "страница 'список приглашённых'" do
        before do
            admin.save
            5.times { FactoryGirl.create(:confirmed_user, inviter_id: admin.id).save }
            2.times { FactoryGirl.create(:confirmed_user, inviter_id: admin.id, anonimity: true).save }
            sign_in admin
            visit invited_list_path
        end

        it "должна содержать ники всех приглашённых, кроме анонимных" do
            admin.childrens.each do |children|
                unless children.anonimity?
                    should have_content(children.login)
                else
                    should_not have_content(children.login)
                end
            end
        end

    end

    describe "страница 'снять деньги'" do
        let(:reach_user) { FactoryGirl.create(:confirmed_user) }
        before do
            reach_user.capital = 288
            reach_user.reload
            sign_in reach_user
            visit withdraw_path
        end

        it { should have_title('снять') }
        it { should have_css('span', text: reach_user.capital) }
    end

end
