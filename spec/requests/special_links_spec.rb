require 'rails_helper'

RSpec.describe "SpecialLinks", :type => :request do
	let(:link) { FactoryGirl.create(:special_link, special_link: "vkontakte") }
	let(:admin) { FactoryGirl.create(:admin) }
	let(:unconfirmed_user) { FactoryGirl.create(:user) }
	let(:new_user) { FactoryGirl.build(:user) }
	let(:confirmed_user) { FactoryGirl.create(:confirmed_user) }

  let(:text_signup) { "Присоединиться!" }

	subject { page }

	before do
		admin.save
		confirmed_user.save
	end

  describe "переход по специальной ссылке" do

  	describe "без указания user_hash" do

  		before { visit "l/#{link.special_link}" }

      it { should have_link(text_signup) }

      describe "создание пользователя" do
      	before do
      		registration(new_user)
      		@user = User.find_by_login(new_user.login)
      	end

      	it { expect(@user).not_to be_nil }

      	it { expect(@user.inviter_id).to eq User.first.id }

        it { expect(@user.special_link_id).to eq link.id }

      end

  	end

  	describe "с указанием user_hash" do

  		before { visit "l/#{link.special_link}/#{confirmed_user.user_hash}" }

      it { should have_link(text_signup) }

      describe "создание пользователя" do
      	before do
      		registration(new_user)
      		@user = User.find_by_login(new_user.login)
      	end

      	it { expect(@user).not_to be_nil }

      	it { expect(@user.inviter_id).to eq confirmed_user.id }

        it { expect(@user.special_link_id).to eq link.id }
      
      end

  	end

    describe "с указанием не существующего user_hash" do

      before { visit "l/#{link.special_link}/#{confirmed_user.user_hash + '2'}" }

      it { should have_link(text_signup) }

      describe "создание пользователя" do
        before do
          registration(new_user)
          @user = User.find_by_login(new_user.login)
        end

        it { expect(@user).not_to be_nil }

        it { expect(@user.inviter_id).to eq User.first.id }

        it { expect(@user.special_link_id).to eq link.id }
      
      end

    end

    describe "с указанием не существующего special_link_id" do

      before { visit "l/#{link.special_link * 2}/#{confirmed_user.user_hash}" }

      it { should have_link(text_signup) }

      describe "создание пользователя" do
        before do
          registration(new_user)
          @user = User.find_by_login(new_user.login)
        end

        it { expect(@user).not_to be_nil }

        it { expect(@user.inviter_id).to eq confirmed_user.id }

        it { expect(@user.special_link_id).to be_nil }
      
      end

    end

  end

end
