require 'rails_helper'

RSpec.describe "AuthenticationPages", :type => :request do
  subject { page }
  let(:user) { FactoryGirl.create(:confirmed_user) }
  let(:admin) { FactoryGirl.create(:admin) }

  before { admin.save }

  describe "страница авторизации" do
  	before { visit signin_path }

  	it { should have_content("Вход") }

  	describe "с инвалидной информацией" do
  		before { click_button "войти" }

  		it { should have_content("Вход") }
  		it { should have_error_message() }

      it { should have_link("О проекте") }
      it { should_not have_link("Помощь") }
  		it { should_not have_link("Профиль") }
  		it { should_not have_link("Пригласить") }
  		it { should_not have_link("Снять деньги") }
      it { should_not have_link("Список приглашённых") }
      it { should_not have_link("Список пользователей") }
  		it { should_not have_link("Управление фондами") }
  		it { should_not have_link("Настройки") }
  		it { should_not have_link("Выход") }

  		describe "после посещения другой страницы" do
  			before { click_link("О проекте") }

  			it { should_not have_error_message }
  		end
  	end

  	describe "с валидной информацией" do
  		before { sign_in user }

  		it { should have_title(user.login) }
  		it { should have_link("Профиль") }
  		it { should have_link("Пригласить") }
  		it { should have_link("Снять деньги") }
  		it { should have_link("Список приглашённых") }
  		it { should have_link("Достижения") }
  		it { should have_link("Настройки") }
  		it { should have_link("Выход") }
      it { should have_link("Помощь") }
  		it { should_not have_link("Вход") }
  		it { should_not have_link("Присоединиться") }
      it { should_not have_link("Статистика и графики") }
      it { should_not have_link("Список пользователей") }
      it { should_not have_link("Управление фондами") }
  	end

    describe "в качестве администратора" do
      before { sign_in admin }

      it { should have_link("Статистика") }
      it { should have_link("Управление фондами") }
      it { should have_link("Список пользователей") }
    end

  end

  describe "для не вошедших пользователей" do

  	describe "в контроллере Users" do

  	end

  	describe "когда user пытается получить доступ к защищённой странице" do

  		before do
  			visit withdraw_url
  			sign_in user
  		end

  		describe "после успешного входа" do

  			it "должна отобразиться ранее недоступная страница" do
  				expect(page).to have_title("снять")
  			end

  		end

  	end

  end

  describe "как неверный пользователь" do
    
    let(:wrong_user) { FactoryGirl.create(:confirmed_user, login: "wrong_user") }
    
    before { sign_in user, no_capybara: true }

    describe "отправка GET запроса к действию Users#edit" do

      before { get edit_user_path(wrong_user) }

      specify { expect(response).to redirect_to(root_url) }

    end

    describe "страница 'специальные ссылки'" do

      before { get special_links_path }

      specify { expect(response).to redirect_to(root_url) }

    end

  end

  describe "как невошедший пользователь" do
    describe "страница 'настройки'" do
      before { get settings_path }

      specify { expect(response).to redirect_to(signin_url) }

    end

    describe "страница 'снять деньги'" do
      before { get withdraw_path }

      specify { expect(response).to redirect_to(signin_url) }
    end

    describe "страница 'список приглашённых'" do
      before { get invited_list_path }

      specify { expect(response).to redirect_to(signin_url) }
    end

    describe "страница 'статистика'" do
      before { get statistics_path }

      specify { expect(response).to redirect_to(signin_url) }
    end

    describe "страница 'специальные ссылки'" do

        before { get special_links_path }

        specify { expect(response).to redirect_to(signin_url) }

    end

  end

  describe "страницы для администраторов" do

    describe "пользователь без прав" do

      before { sign_in user }

      describe "страница 'управление фондами'" do

          before { get funds_path }

          specify { expect(response).to redirect_to(signin_url) }

      end

      describe "страница 'список пользователей'" do

        before { get users_path }

        specify { expect(response).to redirect_to(signin_url) }

      end

      describe "страница 'специальные ссылки'" do

          before { get special_links_path }

          specify { expect(response).to redirect_to(signin_url) }

      end

      describe "страница 'список новостей'" do

          before { get list_news_path }

          specify { expect(response).to redirect_to(signin_url) }

      end

    end

    describe "пользователь с правами" do
      before { sign_in admin }

      describe "страница 'управление фондами'" do

        before { visit funds_path }

        it { should have_title("управление фондами") }

      end

      describe "страница 'список пользователей'" do

        before { visit users_path }

        it { should have_title("список пользователей") }

      end

      describe "страница 'специальные ссылки'" do

          before { visit special_links_path }

          it { should have_title("ссылки") }

      end

      describe "страница 'список новостей'" do

          before { visit list_news_path }

          it { should have_title("список новостей") }

      end

    end

  end

end
